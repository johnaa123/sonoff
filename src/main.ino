#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#define RELAY_PIN 12
#define GLED_PIN 13

#define BTN_PIN 0
#define BTN_PRESSED 0
#define BTN_UNPRESSED 1

#define LED_ON HIGH
#define LED_OFF LOW

#define RELAY_OFF 0
#define RELAY_ON 1

#ifndef VERSION
#define VERSION "1"
#endif

#ifndef WIFI_SSID
#define WIFI_SSID "your ssid"
#endif

#ifndef WIFI_PASS
#define WIFI_PASS "your wifi password"
#endif

#ifndef OTA_PASS
#define OTA_PASS "an ota pass"
#endif

#ifndef PROJECT_NAME
#define PROJECT_NAME "project"
#endif

#ifndef MQTT_SERVER
#define MQTT_SERVER "test.mosquitto.org"
#endif

#define MQTT_PORT 1883

char GROUP_TOPIC[128];
char COMMAND_TOPIC[128];
char STATE_TOPIC[128];
char LOG_TOPIC[128];

char INSTANCE_NAME[16];

WiFiClient wclient;
PubSubClient mqttClient(wclient);

struct State {
    char button = BTN_UNPRESSED;
    unsigned long lastPress = 0;
    char relay = RELAY_OFF;
} state;


void mqttPublish(String topic, String payload) {
    char chPayload[500];
    char chTopic[100];

    if (!mqttClient.connected()) {
        return;
    }

    topic.toCharArray(chTopic, 100);
    ("(" + String(millis()) + " - " + WiFi.localIP().toString() + ") " + INSTANCE_NAME + ": " + payload).toCharArray(chPayload, 100);
    mqttClient.publish(chTopic, chPayload);
}


void connectMQTT() {
    if (mqttClient.connected()) {
        mqttClient.loop();
        return;
    }

    mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
    mqttClient.setCallback(mqttCallback);

    int retries = 4;
    Serial.println("\nConnecting to MQTT...");
    while (!mqttClient.connect(INSTANCE_NAME) && retries--) {
        delay(500);
        Serial.println("Retry...");
    }

    if (!mqttClient.connected()) {
        Serial.println("\nfatal: MQTT server connection failed. Rebooting.");
        delay(200);
        ESP.restart();
    }

    mqttClient.subscribe(COMMAND_TOPIC);
    mqttClient.subscribe(GROUP_TOPIC);

    Serial.println("Connected.");
    mqttPublish(LOG_TOPIC, "Connected.");
}


// Receive a message from MQTT and act on it.
void mqttCallback(char* chTopic, byte* chPayload, unsigned int length) {
    chPayload[length] = '\0';
    parseCommand((char *)chPayload);
}


#ifdef OTA_SERVER
void doHTTPUpdate() {
    if (WiFi.status() != WL_CONNECTED) return;

    t_httpUpdate_return ret = ESPhttpUpdate.update("http://" OTA_SERVER "/" PROJECT_NAME "/", VERSION);

    switch(ret) {
        case HTTP_UPDATE_FAILED:
            Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\r\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
            break;

        case HTTP_UPDATE_NO_UPDATES:
            Serial.println("HTTP_UPDATE_NO_UPDATES");
            break;

        case HTTP_UPDATE_OK:
            Serial.println("HTTP_UPDATE_OK");
            break;
    }
}
#endif


void connectWifi() {
    if (WiFi.waitForConnectResult() == WL_CONNECTED) {
        return;
    }

    WiFi.mode(WIFI_STA);
    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
        WiFi.begin(WIFI_SSID, WIFI_PASS);
        Serial.println("Connecting to wifi...");
    }
}


void parseCommand(char* cmd) {
    String command = String((char*)cmd);

    Serial.println(String("Command ") + command + String(" received."));

    if (command == "power on" || command == "relay on") {
        relayOn();
    } else if (command == "power off" || command == "relay off") {
        relayOff();
    } else if (command == "power toggle" || command == "relay toggle") {
        relayToggle();
    } else if (command == "status" || command == "state") {
        publishState();
    } else if (command == "reboot") {
        ESP.reset();
    }
}


void publishState() {
    if (state.relay == RELAY_ON) {
        Serial.println("Power on.");
        mqttPublish(STATE_TOPIC, "power on");
    } else {
        Serial.println("Power off.");
        mqttPublish(STATE_TOPIC, "power off");
    }
}


void relayOn() {
    digitalWrite(RELAY_PIN, RELAY_ON);
    state.relay = RELAY_ON;
    pinMode(GLED_PIN, LED_ON);
    publishState();
}


void relayOff() {
    digitalWrite(RELAY_PIN, RELAY_OFF);
    state.relay = RELAY_OFF;
    pinMode(GLED_PIN, LED_OFF);
    publishState();
}


void relayToggle() {
    if (state.relay == RELAY_OFF) {
        relayOn();
    } else {
        relayOff();
    }
}


void checkButton() {
    char btn = digitalRead(BTN_PIN);

    if (btn == state.button || (state.lastPress > millis() - 50)) return;

    if (btn == BTN_PRESSED) {
        relayToggle();
    }

    state.button = btn;
    state.lastPress = millis();
}


void resetPins() {
    pinMode(1, OUTPUT);
    analogWrite(1, 0);
    pinMode(2, OUTPUT);
    analogWrite(2, 0);
    pinMode(3, OUTPUT);
    analogWrite(3, 0);
    pinMode(4, OUTPUT);
    analogWrite(4, 0);
    pinMode(5, OUTPUT);
    analogWrite(5, 0);
    pinMode(12, OUTPUT);
    analogWrite(12, 0);
    pinMode(13, OUTPUT);
    analogWrite(13, 0);
    pinMode(14, OUTPUT);
    analogWrite(14, 0);
    pinMode(15, OUTPUT);
    analogWrite(15, 0);
}


void setup() {
    resetPins();

    pinMode(RELAY_PIN, OUTPUT);
    pinMode(GLED_PIN, OUTPUT);
    pinMode(BTN_PIN, INPUT);

    analogWrite(RELAY_PIN, RELAY_OFF);
    pinMode(GLED_PIN, LOW);

    Serial.begin(115200);
    Serial.println("\n\nBooting version " VERSION "...");

    snprintf(GROUP_TOPIC, 128, PROJECT_NAME "/all/command");
    snprintf(COMMAND_TOPIC, 128, PROJECT_NAME "/%08X/command", ESP.getChipId());
    snprintf(STATE_TOPIC, 128, PROJECT_NAME "/%08X/state", ESP.getChipId());
    snprintf(LOG_TOPIC, 128, PROJECT_NAME "/%08X/log", ESP.getChipId());

    snprintf(INSTANCE_NAME, 16, "%08X", ESP.getChipId());

    connectWifi();
#ifdef OTA_SERVER
    doHTTPUpdate();
#endif
    connectMQTT();

    ArduinoOTA.setHostname((String(PROJECT_NAME "-") + String(INSTANCE_NAME)).c_str());
    ArduinoOTA.setPassword((const char *) OTA_PASS);

    ArduinoOTA.onStart([]() {
        resetPins();
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
    });
    ArduinoOTA.begin();
}


void loop() {
    connectWifi();
    connectMQTT();
    checkButton();
    ArduinoOTA.handle();
}
